import java.io.*;

public class FindWords {

    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите название файла");
        String fileName = bufferedReader.readLine();
        FileReader fileReader = new FileReader(fileName);
        System.out.println("Введите искомое слово");
        String word = bufferedReader.readLine();
        StringBuilder stringBuilder = new StringBuilder();
        int count = 0;

        while (fileReader.ready()) {
            stringBuilder.append((char) fileReader.read());
        }

        String s = stringBuilder.toString();
        String[] parts = s.split("[^A-Za-z0-9]");

        for (int i = 0; i<parts.length; i++) {
            if (parts[i].equals(word)) {
                count++;
            }
        }

        fileReader.close();
        bufferedReader.close();
        System.out.println("Слово " + word + " в файле " + fileName + " встречается " + count + " раз");
    }
}
